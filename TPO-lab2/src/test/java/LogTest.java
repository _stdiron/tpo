import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.stpdiron.Ln;
import org.stpdiron.Log;
import org.stpdiron.PrecisionableCalculation;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class LogTest {
    final double DEFAULT_PRECISION = 0.0001;
    @Mock
    PrecisionableCalculation lnMock;
    @Spy
    PrecisionableCalculation lnSpy;

    @Test
    void checkLnCall(){
        var log = new Log(DEFAULT_PRECISION, lnSpy);
        log.calculate(10);
        verify(lnSpy, times(2)).calculate(any(Double.class));
    }

    @Test
    void log2test() {
        Mockito.when(lnMock.calculate(2.0)).thenReturn(0.693147181);
        Mockito.when(lnMock.calculate(0.5)).thenReturn(-0.693147181);
        Mockito.when(lnMock.calculate(1.0)).thenReturn(0.0);
        Mockito.when(lnMock.calculate(5.0)).thenReturn(1.609437912);
        var log = new Log(2.0, lnMock);
        assertEquals(log.calculate(0.5), -1.0, DEFAULT_PRECISION);
        assertEquals(log.calculate(1.0), 0.0, DEFAULT_PRECISION);
        assertEquals(log.calculate(5.0), 2.32192, DEFAULT_PRECISION);
    }

    @Test
    void log3Test() {
        Mockito.when(lnMock.calculate(3.0)).thenReturn(Math.log(3.0));
        Mockito.when(lnMock.calculate(0.5)).thenReturn(Math.log(0.5));
        Mockito.when(lnMock.calculate(1.0)).thenReturn(0.0);
        Mockito.when(lnMock.calculate(81.0)).thenReturn(Math.log(81.0));
        var log = new Log(3.0, lnMock);
        assertEquals(log.calculate(0.5), -0.630930, DEFAULT_PRECISION);
        assertEquals(log.calculate(1.0), 0.0, DEFAULT_PRECISION);
        assertEquals(log.calculate(81.0), 4.0, DEFAULT_PRECISION);
    }

    @Test
    void log10Test() {
        Mockito.when(lnMock.calculate(10.0)).thenReturn(Math.log(10));
        Mockito.when(lnMock.calculate(0.5)).thenReturn(Math.log(0.5));
        Mockito.when(lnMock.calculate(1.0)).thenReturn(0.0);
        Mockito.when(lnMock.calculate(90.0)).thenReturn(Math.log(90));
        Mockito.when(lnMock.calculate(100.0)).thenReturn(Math.log(100));
        var log = new Log(10.0, lnMock);
        assertEquals(log.calculate(0.5), Math.log10(0.5), DEFAULT_PRECISION);
        assertEquals(log.calculate(1.0), 0.0, DEFAULT_PRECISION);
        assertEquals(log.calculate(90), Math.log10(90), DEFAULT_PRECISION);
        assertEquals(log.calculate(100), 2, DEFAULT_PRECISION);
    }

    @Test
    void logIntegrationTest() {
        var ln = new Ln(DEFAULT_PRECISION);
        for (double x = 0.5; x < 10; x += 0.1) {
            for (int base = 2; x <= 10; x += 1) {
                var log = new Log((double) base, ln);
                assertEquals(Math.log(x) / Math.log(base), log.calculate(x), DEFAULT_PRECISION);
            }
        }
    }
}
