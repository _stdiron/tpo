import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.stpdiron.FunctionSystem;
import org.stpdiron.PrecisionableCalculation;

import static java.lang.Double.NaN;
import static java.lang.Double.POSITIVE_INFINITY;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FunctionSystemTest {
    final double DEFAULT_PRECISION = 0.0001;

    double calculateWithJavaMath(double x) {
        if (x <= 0) {
            double t = Math.abs(x % Math.PI);
            if (t <= DEFAULT_PRECISION*10 || (t >= Math.PI - DEFAULT_PRECISION*10 && t <= Math.PI + DEFAULT_PRECISION*10))
                return POSITIVE_INFINITY;
            return 1 / Math.sin(x) / Math.sin(x);
        } else {
            double log2Expected = Math.log(x) / Math.log(2);
            double log3Expected = Math.log(x) /  Math.log(3);
            return Math.pow(log3Expected / log2Expected, 9)
                    / Math.pow(log2Expected * log3Expected, 2) * Math.log10(x);
        }
    }

    @Nested
    class ModuleTest {
        @Mock
        private PrecisionableCalculation sinMock, cscMock, log2Mock, log3Mock, log10Mock;
        @Spy
        private PrecisionableCalculation sinSpy, cscSpy, log2Spy, log3Spy, log10Spy;

        @Test
        void testStubCallsLessThan0x() {
            var f = new FunctionSystem(sinSpy, cscSpy, log2Spy,
                    log3Spy, log10Spy, DEFAULT_PRECISION);
            f.calculate(-1.0);
            verify(log3Spy, times(0)).calculate(any(Double.class));
            verify(log2Spy, times(0)).calculate(any(Double.class));
            verify(log10Spy, times(0)).calculate(any(Double.class));
            verify(sinSpy, times(1)).calculate(any(Double.class));
            verify(cscSpy, times(1)).calculate(any(Double.class));
        }

        @Test
        void testStubCallsBiggerThan0x() {
            var f = new FunctionSystem(sinSpy, cscSpy, log2Spy,
                    log3Spy, log10Spy, DEFAULT_PRECISION);
            f.calculate(1.0);
            verify(log3Spy, times(2)).calculate(any(Double.class));
            verify(log2Spy, times(2)).calculate(any(Double.class));
            verify(log10Spy).calculate(any(Double.class));
            verify(sinSpy, times(0)).calculate(any(Double.class));
            verify(cscSpy, times(0)).calculate(any(Double.class));
        }

        @Test
        void testLessThan0x() {
            var f = new FunctionSystem(sinMock, cscMock, log2Mock,
                    log3Mock, log10Mock, DEFAULT_PRECISION);
            when(cscMock.calculate(-2.0)).thenReturn(-1.0997501702946164);
            when(sinMock.calculate(-2.0)).thenReturn(-0.9092974268256817);
            double expected = 1 / Math.sin(-2) / Math.sin(-2);
            assertEquals(expected, f.calculate(-2.0), DEFAULT_PRECISION);
        }

        @Test
        void testBiggerThan0x() {
            var f = new FunctionSystem(sinMock, cscMock, log2Mock,
                    log3Mock, log10Mock, DEFAULT_PRECISION);
            double x = 2;
            double log2Expected = Math.log(x) / Math.log(2);
            double log3Expected = Math.log(x) /  Math.log(3);
            when(log2Mock.calculate(x)).thenReturn(log2Expected);
            when(log3Mock.calculate(x)).thenReturn(log3Expected);
            when(log10Mock.calculate(x)).thenReturn(Math.log10(x));
            double expected = Math.pow(log3Expected / log2Expected, 9)
                    / Math.pow(log2Expected * log3Expected, 2) * Math.log10(x);
            assertEquals(expected, f.calculate(x), DEFAULT_PRECISION);
        }
    }

    @Nested
    class IntegrationTest {
        final FunctionSystem f = new FunctionSystem(DEFAULT_PRECISION);

        @Test
        void testZero() {
            assertEquals(POSITIVE_INFINITY, f.calculate(0));
        }

        @Test
        void testLessThanZero() {
            double x = -30;
            while (x <= 0) {
                assertEquals(calculateWithJavaMath(x), f.calculate(x), DEFAULT_PRECISION);
                x += 0.01;
            }
        }

        @ParameterizedTest
        @ValueSource(doubles = {0.01, 0.001, 0.5})
        void testOnExtraSmallPositives(double arg) {
            assertEquals(calculateWithJavaMath(arg), f.calculate(arg), DEFAULT_PRECISION);
        }

        @Test
        void testBiggerThanZero() {
            double x = 1;
            while (x <= 30) {
                assertEquals(calculateWithJavaMath(x), f.calculate(x), DEFAULT_PRECISION);
                x += 0.01;
            }
        }
    }

}
