import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.stpdiron.Csc;
import org.stpdiron.Ln;
import org.stpdiron.Log;
import org.stpdiron.Sin;

import static java.lang.Double.POSITIVE_INFINITY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CscTest {
    final double DEFAULT_PRECISION = 0.0001;
    @Mock
    Sin sinMock;
    @Spy
    Sin sinSpy;
    Csc csc;

    @BeforeEach
    void setup() {
        csc = new Csc(sinMock);
    }

    @Test
    void checkSinCall(){
        var mcsc = new Csc(sinSpy);
        mcsc.calculate(2.3);
        verify(sinSpy).calculate(any(Double.class));
    }

    @Test
    void testOnZeroAndPi() {
        Mockito.when(sinMock.calculate(0)).thenReturn(0.0);
        Mockito.when(sinMock.calculate(Math.PI)).thenReturn(0.0);
        Mockito.when(sinMock.calculate(-Math.PI)).thenReturn(0.0);
        assertEquals(POSITIVE_INFINITY, csc.calculate(0), DEFAULT_PRECISION);
        assertEquals(POSITIVE_INFINITY, csc.calculate(Math.PI), DEFAULT_PRECISION);
        assertEquals(POSITIVE_INFINITY, csc.calculate(-Math.PI), DEFAULT_PRECISION);
    }

    @Test
    void testOnHalfOfPi() {
        Mockito.when(sinMock.calculate(Math.PI / 2)).thenReturn(1.0);
        assertEquals(1.0, csc.calculate(Math.PI / 2), DEFAULT_PRECISION);
    }

    @Test
    void testOnHalfOfPiNeg() {
        Mockito.when(sinMock.calculate(-Math.PI / 2)).thenReturn(-1.0);
        assertEquals(-1.0, csc.calculate(-Math.PI / 2), DEFAULT_PRECISION);
    }

    @Test
    void testOnOfPiDiv4() {
        Mockito.when(sinMock.calculate(Math.PI / 4)).thenReturn(Math.sqrt(2) / 2);
        assertEquals(1 / (Math.sqrt(2) / 2), csc.calculate(Math.PI / 4), DEFAULT_PRECISION);
    }

    @Test
    void testOnOfPiDiv4Neg() {
        Mockito.when(sinMock.calculate(-Math.PI / 4)).thenReturn(-Math.sqrt(2) / 2);
        assertEquals(-1 / (Math.sqrt(2) / 2), csc.calculate(-Math.PI / 4), DEFAULT_PRECISION);
    }

    @Test
    void cscIntegrationTest() {
        var sin = new Sin(DEFAULT_PRECISION);
        for (double x = 0.5; x < 10; x += 0.1) {
            var csc = new Csc(sin);
            assertEquals(1/Math.sin(x), csc.calculate(x), DEFAULT_PRECISION);
        }
    }

}
