import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;
import org.stpdiron.Sin;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class SinTest {

    final double DEFAULT_PRECISION = 0.0001;
    final Sin sin = new Sin(DEFAULT_PRECISION);

    @Test
    void testOnZero() {
        assertEquals(Math.sin(0), sin.calculate(0), DEFAULT_PRECISION);
    }


    @ParameterizedTest()
    @ValueSource(doubles = {Math.PI / 2, -Math.PI / 2})
    void testOnHalfPi(double arg) {
        assertEquals(Math.sin(arg), sin.calculate(arg), DEFAULT_PRECISION);
    }

    @ParameterizedTest()
    @ValueSource(doubles = {Math.PI, -Math.PI})
    void testOnPi(double arg) {
        assertEquals(Math.sin(arg), sin.calculate(arg), DEFAULT_PRECISION);
    }

    @Test
    void testInPeriodPositive() {
        double x = 0;
        while (x < Math.PI) {
            assertEquals(Math.sin(x), sin.calculate(x), DEFAULT_PRECISION);
            x += 0.01;
        }
    }

    @Test
    void testOutOfPeriodPositive() {
        double x = Math.PI;
        while (x < 3*Math.PI) {
            assertEquals(Math.sin(x), sin.calculate(x), DEFAULT_PRECISION);
            x += 0.01;
        }
    }

    @Test
    void testInPeriodNegative() {
        double x = 0;
        while (x > -Math.PI) {
            assertEquals(Math.sin(x), sin.calculate(x), DEFAULT_PRECISION);
            x -= 0.01;
        }
    }

    @Test
    void testOutOfPeriodNegative() {
        double x = -Math.PI;
        while (x > -3*Math.PI) {
            assertEquals(Math.sin(x), sin.calculate(x), DEFAULT_PRECISION);
            x -= 0.01;
        }
    }

}
