import org.junit.jupiter.api.Test;
import org.stpdiron.BinPow;

import static java.lang.Double.NaN;
import static org.junit.jupiter.api.Assertions.*;

public class BinPowTest {
    final double DEFAULT_PRECISION = 0.0001;

    @Test
    void testOnZero() {
        for (int i = 1; i < 100; i++) {
            var pow = new BinPow(i);
            assertEquals(pow.calculate(0), 1);
        }
    }

    @Test
    void testOnOne() {
        for (int i = 1; i < 100; i++) {
            var pow = new BinPow(i);
            assertEquals(pow.calculate(1), i);
        }
    }

    @Test
    void testOnNegatives() {
        var pow = new BinPow(10);
        assertThrowsExactly(ArithmeticException.class, () -> pow.calculate(-2));
        assertThrowsExactly(ArithmeticException.class, () -> pow.calculate(-5));
        assertThrowsExactly(ArithmeticException.class, () -> pow.calculate(-10));
    }

    @Test
    void testVariousNumbers() {
        for (int i = 1; i < 100; i++) {
            var pow = new BinPow(i);
            assertEquals(pow.calculate(2), Math.pow(i, 2));
            assertEquals(pow.calculate(3), Math.pow(i, 3));
            assertEquals(pow.calculate(5), Math.pow(i, 5));
            assertEquals(pow.calculate(10), Math.pow(i, 10));
        }
    }

    @Test
    void testBigNumbers() {
        for (int i = 1; i < 50; i++) {
            var pow = new BinPow(i);
            assertEquals(pow.calculate(10), Math.pow(i, 10), DEFAULT_PRECISION);
            assertEquals(pow.calculate(12), Math.pow(i, 12), DEFAULT_PRECISION);
            assertEquals(pow.calculate(15), Math.pow(i, 15), DEFAULT_PRECISION);
        }
    }
}
