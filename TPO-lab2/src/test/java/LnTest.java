import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.stpdiron.Ln;

import static java.lang.Double.NaN;
import static org.junit.jupiter.api.Assertions.*;


public class LnTest {

    final double DEFAULT_PRECISION = 0.0001;
    Ln ln = new Ln(DEFAULT_PRECISION);

    @ParameterizedTest
    @ValueSource(doubles = {0.1, 0.5, 1.0, 5.0, 10.0})
    void testSmallNumbers(double arg) {
        assertEquals(Math.log(arg), ln.calculate(arg), DEFAULT_PRECISION);
    }

    @ParameterizedTest
    @ValueSource(doubles = {100.0, 1000.0, 10000.0})
    void testBigNumbers(double arg) {
        assertEquals(Math.log(arg), ln.calculate(arg), DEFAULT_PRECISION);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-10, -1, 0})
    void testNotInRange(double arg) {
        assertEquals(NaN, ln.calculate(arg), DEFAULT_PRECISION);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.001, 0.0001})
    void testExtraSmall(double arg) {
        assertEquals(Math.log(arg), ln.calculate(arg), DEFAULT_PRECISION);
    }

    @Test
    void iterationTest() {
        double x = 0.5;
        while (x < 10.0) {
            assertEquals(Math.log(x),ln.calculate(x), DEFAULT_PRECISION);
            x += 0.01;
        }
    }
}
