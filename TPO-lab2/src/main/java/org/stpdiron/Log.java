package org.stpdiron;

public class Log extends PrecisionableCalculation {
    protected final PrecisionableCalculation ln;
    private final double den;
    public Log(double base, PrecisionableCalculation ln) {
        super(ln.precision);
        this.ln = ln;
        this.den = ln.calculate(base);
    }

    @Override
    public double calculate(double x) {
        return ln.calculate(x) / den;
    }
}
