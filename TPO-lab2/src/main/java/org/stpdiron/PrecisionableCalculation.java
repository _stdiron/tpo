package org.stpdiron;

public abstract class PrecisionableCalculation implements Calculation{

    protected double precision;
    public PrecisionableCalculation(double precision) {
        this.precision = precision;
    }
    public PrecisionableCalculation() {
        this.precision = 0.0001;
    }
}
