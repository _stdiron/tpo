package org.stpdiron;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        var functions = new ArrayList<Calculation>();
        functions.add(new FunctionSystem(0.0001));
        functions.add(new Ln(0.0001));
        functions.add(new Sin(0.0001));
        functions.add(new Csc(new Sin(0.0001)));
        for (Calculation f : functions) {
            var writer = new CsvWriter(f);
            writer.write(-10, 10, 0.001);
        }

    }
}