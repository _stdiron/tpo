package org.stpdiron;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.Double.NaN;

public class Ln extends PrecisionableCalculation{
    public Ln(double precision) {
        super(precision);
    }

    @Override
    public double calculate(double v) {
        return method2(v);
    }

    private double method2(double v) {
        if (v < precision)
            return NaN;
        double x = (v - 1) / (v + 1);
        double sum = x;
        double current = x;
        int iters = (int) (1 / precision)*10;
        for (int i = 3, j = 1; i < iters; i += 2) {
            for (; j < i; j++) {
                current *= x;
            }
            sum += current / i;
        }
        return sum*2;
    }

    private BigDecimal method(double v) {
        BigDecimal x = BigDecimal.valueOf(v - 1);
        x = x.divide(BigDecimal.valueOf(v + 1), RoundingMode.UNNECESSARY);
        BigDecimal current = x;
        BigDecimal sum = x;
        int iters = (int) (1 / precision);
        for (int i = 3, j = 1; i < iters; i += 2) {
            for (; j < i; j++) {
                current = current.multiply(x);
            }
            sum = sum.add(current.divide(BigDecimal.valueOf(i), RoundingMode.UNNECESSARY));
        }
        return sum.multiply(BigDecimal.valueOf(2));
    }
}
