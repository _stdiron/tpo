package org.stpdiron;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

public class CsvWriter {
    private final Calculation f;
    private final String name;
    public CsvWriter(Calculation f) {
        this.f = f;
        name = f.getClass().getSimpleName();
    }

    public CsvWriter(Calculation f, String fname) {
        this.f = f;
        name = fname;
    }

    public void write(double start, double stop, double step) throws IOException {
        String spath = String.format("src/test/resources/%s.csv", name);
        var path = Paths.get(spath);
        var file = new File(path.toUri());
        if (file.exists())
            file.delete();
        file.createNewFile();
        var printWriter = new PrintWriter(file);
        printWriter.write("y; x\n");
        for (double x = start; x < stop; x += step) {
//            double y = Math.max(Math.min(f.calculate(x), 10000), -10000);
            double y = f.calculate(x);
            printWriter.write(
                    String.format("%f; %f\n", y, x).replace(",", ".")
            );
        }
        printWriter.close();
    }
}
