package org.stpdiron;

public class BinPow implements Calculation {
    private final double base;
    public BinPow(double base) {
        this.base = base;
    }


    // только для положительных целых степеней
    @Override
    public double calculate(double x) {
        double result = 1;
        long n = (long )x;
        double a = base;
        if (x < 0)
            throw new ArithmeticException("BinPow cant work properly with negative power");
        while (n != 0) {
            if (n % 2 == 1) {
                result *= a;
            }
            a *= a;
            n >>= 1;
        }
        return result;
    }
}
