package org.stpdiron;

import static java.lang.Double.POSITIVE_INFINITY;

public class Csc extends PrecisionableCalculation{
    private final PrecisionableCalculation sin;
    public Csc(PrecisionableCalculation sin) {
        this.sin = sin;
    }

    @Override
    public double calculate(double x) {
        return 1/sin.calculate(x);
    }
}
