package org.stpdiron;

import static java.lang.Double.POSITIVE_INFINITY;

public class Sin extends PrecisionableCalculation{

    public Sin() {
        super(0.0001);
    }
    public Sin(double precision) {
        super(precision);
    }
    public double calculate(double x) {
        x %= 2*Math.PI;
        double sum = x;
        double current = 1;
        int flag = -1;
        for (int i = 3; Math.abs(current) > precision*0.0000001; i+=2){
            current = 1;
            for (int j = 1; j <= i; j++){
                current *= x;
                current /= j;
            }
            sum += flag*current;
            flag = -flag;
        }
        return sum;
    }
}
