package org.stpdiron;

import static java.lang.Double.POSITIVE_INFINITY;

public class FunctionSystem extends PrecisionableCalculation{
    private final PrecisionableCalculation sin, csc, log2, log3, log10;
    public FunctionSystem(
            PrecisionableCalculation sin,
            PrecisionableCalculation csc,
            PrecisionableCalculation log2,
            PrecisionableCalculation log3,
            PrecisionableCalculation log10,
            double precision) {
        super(precision);
        this.sin = sin;
        this.csc = csc;
        this.log2 = log2;
        this.log3 = log3;
        this.log10 = log10;
    }

    public FunctionSystem(double precision) {
        super(precision);
        sin = new Sin(precision);
        csc = new Csc(sin);
        var ln = new Ln(precision);
        log2 = new Log(2, ln);
        log3 = new Log(3, ln);
        log10 = new Log(10, ln);
    }

    @Override
    public double calculate(double x) {
        if (x <= 0)
            return func1(x);
        else
            return func2(x);
    }

    private double func1(double x) {
        double t = Math.abs(x % Math.PI);
        if (t <= precision*10 || (t >= Math.PI - precision*10 && t <= Math.PI + precision*10))
            return POSITIVE_INFINITY;
        return csc.calculate(x) / sin.calculate(x);
    }

    private double func2(double x) {
        double p1 = log3.calculate(x) / log2.calculate(x);
        double p2 = log3.calculate(x) * log2.calculate(x);
        p1 = new BinPow(p1).calculate(3);
        p1 = new BinPow(p1).calculate(3);
        p2 = new BinPow(p2).calculate(2);
        return p1 / p2 * log10.calculate(x);
    }
}
