package org.stpdiron;

public interface Calculation {
    double calculate(double x);
}
