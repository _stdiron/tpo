package org.stpdiron;

public class ArcCos {
    private final double accuracy;
    public ArcCos(double accuracy) {
        this.accuracy = accuracy*accuracy;
    }

    public double calculate(double x) {
        if (Math.abs(x) > 1)
            return Double.NaN;

        double answer = x, sum = 1;
        double n = 1;
        double n2_fact = 2, n_fact = 1;

        while (Math.abs(sum) > accuracy) {
            sum = n2_fact / n_fact / n_fact / Math.pow(4,n) / (2*n + 1)
                    * Math.pow(x, 2*n+1);
            n += 1;
            n_fact *= n;
            double nn = (n-1)*2 + 1;
            while (n*2-nn > -0.1) {
                n2_fact *= nn;
                nn += 1;
            }
            answer += sum;
        }

        return Math.PI/2 - answer;
    }
}
