package org.stpdiron.task3;

import java.util.*;

public class Environment {
    private Map<Coordinates, WorldObject> placed;
    private Deque<WorldObject> playQueue;
    public Environment() {
        playQueue = new LinkedList<>();
        placed = new HashMap<>();
    }

    public <T extends WorldObject> void place(int x, int y, T object) {
        if (placed.containsKey(new Coordinates(x, y))) {
            throw new EnvironmentException("cant place it here");
        }
        placed.put(new Coordinates(x, y), object);
        playQueue.add(object);
    }

    public <T extends WorldObject> T get(int x, int y) {
        if (!placed.containsKey(new Coordinates(x, y)))
            return null;
        return (T)placed.get(new Coordinates(x, y));
    }

    public <T extends WorldObject> T get(String name) {
        for (WorldObject wo : placed.values()) {
           if (wo.getName() == name)
               return (T) wo;
        }
        return null;
    }

    Coordinates getPosition(WorldObject object) {
        for (Map.Entry<Coordinates, WorldObject> entry : placed.entrySet()) {
            if (entry.getValue() == object)
                return entry.getKey();
        }
        return null;
    }

    public void play() {
        for (WorldObject worldObject : playQueue) {
            if (worldObject.isBroken())
                continue;
            worldObject.play(this);
        }
    }

    static class Coordinates {
        public int x;
        public int y;
        public Coordinates(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Coordinates{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Coordinates that = (Coordinates) o;
            return x == that.x && y == that.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }
}
