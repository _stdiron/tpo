package org.stpdiron.task3;

public class Bomb extends WorldObject{
    private int damage;
    public Bomb(int damage) {
        super("Бомба", 10);
        this.damage = damage;
    }

    @Override
    public void onBreak(Environment e) {
        System.out.println("[Бомба взорвана]");
        Environment.Coordinates coordinates = e.getPosition(this);
        for (int x = coordinates.x-10; x < coordinates.x+10; x++) {
            for (int y = coordinates.y-10; y < coordinates.y+10; y++) {
                WorldObject object = e.get(x, y);
                if (object != null)
                    object.damage(damage, e);
            }
        }
    }

    @Override
    public void play(Environment e) {
        // break to make an explosion
        damage(damage, e);
    }
}
