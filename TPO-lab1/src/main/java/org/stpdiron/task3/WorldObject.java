package org.stpdiron.task3;

import java.util.Objects;

public abstract class WorldObject  {
    private String name;
    private int strength;
    public WorldObject(String name, int strength) {
        this.name = name;
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }

    public String getName() {
        return name;
    }

    public void damage(int dmg, Environment e) {
        if (isBroken())
            return;
        strength = Math.max(strength - dmg, 0);
        if (isBroken())
            onBreak(e);
    }

    public abstract void onBreak(Environment e);

    public abstract void play(Environment e);

    public boolean isBroken() {
        return strength == 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WorldObject) {
            return Objects.equals(name, ((WorldObject) obj).name);
        }
        return false;
    }
}
