package org.stpdiron.task3;

public class ComputerBank extends WorldObject{
    public ComputerBank() {
        super("Компьютерный банк", 100);
    }

    @Override
    public void onBreak(Environment e) {
        System.out.println("[Комьютерный банк: ...]");
    }

    @Override
    public void play(Environment e) {
        System.out.println("[Комьютерный банк: работает]");
    }
}
