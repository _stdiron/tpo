package org.stpdiron.task3;

public class EnvironmentException extends RuntimeException{
    public EnvironmentException(String message) {
        super(message);
    }
}
