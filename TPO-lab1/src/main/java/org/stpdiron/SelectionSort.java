package org.stpdiron;

import java.util.Arrays;

public class SelectionSort {
    public static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int imin = i;
            for (int j = i+1; j < array.length; j++) {
                if (array[imin] > array[j])
                    imin = j;
            }
            int t = array[i];
            array[i] = array[imin];
            array[imin] = t;
        }
    }
}
