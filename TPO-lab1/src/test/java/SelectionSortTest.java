import org.junit.jupiter.api.Test;
import org.stpdiron.SelectionSort;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SelectionSortTest {
    @Test
    void sortEmpty() {
        int[] a = new int[] {};
        SelectionSort.sort(a);
        assertArrayEquals(new int[] {}, a);
    }

    @Test
    void sortAlmostSorted() {
        int[] a = new int[] {1,2,3,4,20,6,7,8,10};
        int[] b = new int[] {1,20,3,4,5,6,-7,8,10};
        int[] c = new int[] {1,2,3,24,4,-3,5,7,50};
        SelectionSort.sort(a);
        SelectionSort.sort(b);
        SelectionSort.sort(c);
        assertArrayEquals(new int[] {1,2,3,4,6,7,8,10,20}, a);
        assertArrayEquals(new int[] {-7,1,3,4,5,6,8,10,20}, b);
        assertArrayEquals(new int[] {-3, 1,2,3,4,5,7,24,50}, c);
    }

    @Test
    void sortWithDuplicates() {
        int[] a = new int[] {1,1,1,6,7,7,-10,8,-10};
        int[] b = new int[] {2, 2, 7, 8, 2, 2, 2, 1, 0};
        int[] c = new int[] {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
        SelectionSort.sort(a);
        SelectionSort.sort(c);
        SelectionSort.sort(b);
        assertArrayEquals(new int[] {-10,-10,1,1,1,6,7,7,8}, a);
        assertArrayEquals(new int[] {0,1,2,2,2,2,2,7,8}, b);
        assertArrayEquals(new int[] {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5}, c);
    }

    @Test
    void sortBigNumbers() {
        int[] a = new int[] {5345345,2353234,23423423,2124132,8678678,432342,86786};
        int[] b = new int[] {3466453,98764354,23557654,1343424,8767963,123123};
        int[] aSorted = a.clone();
        int[] bSorted = b.clone();
        Arrays.sort(aSorted);
        Arrays.sort(bSorted);
        SelectionSort.sort(a);
        SelectionSort.sort(b);
        assertArrayEquals(aSorted, a);
        assertArrayEquals(bSorted, b);
    }

    @Test
    void sortBigNegativeNumbers() {
        int[] a = new int[] {-5345345,-2353234,-23423423,-2124132,-8678678,-432342,-836786};
        int[] b = new int[] {-23466453,-598764354,-2357654,-1343424,-8767963,-12312314};
        int[] aSorted = a.clone();
        int[] bSorted = b.clone();
        Arrays.sort(aSorted);
        Arrays.sort(bSorted);
        SelectionSort.sort(a);
        SelectionSort.sort(b);
        assertArrayEquals(aSorted, a);
        assertArrayEquals(bSorted, b);
    }

}
