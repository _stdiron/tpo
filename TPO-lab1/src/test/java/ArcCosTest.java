import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.stpdiron.ArcCos;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArcCosTest {
    @ParameterizedTest
    @ValueSource(doubles = {1.1, 10, 1000})
    void notInRangePositive(double x) {
        ArcCos acos = new ArcCos(0.01);
        assertEquals(acos.calculate(x), Double.NaN, 0);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-1.1, -10, -1000})
    void notInRangeNegative(double x) {
        ArcCos acos = new ArcCos(0.01);
        assertEquals(acos.calculate(x), Double.NaN, 0);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-0.99, 0.99})
    void acceptableRangeBounds(double x) {
        ArcCos acos = new ArcCos(0.01);
        assertEquals(acos.calculate(x), Math.acos(x), 0.01);
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.1, 0.01})
    void iterateInRangeWithVariousAccuracy(double acc) {
        double val = -0.99;
        ArcCos acos = new ArcCos(acc);
        while (val < 0.99) {
            assertEquals(acos.calculate(val), Math.acos(val), acc);
            val += 0.01;
        }
    }
}
