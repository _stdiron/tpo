import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.stpdiron.task3.Bomb;
import org.stpdiron.task3.ComputerBank;
import org.stpdiron.task3.Environment;
import org.stpdiron.task3.EnvironmentException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class DomainModelTest {
    private final PrintStream stdin = System.out;
    private ByteArrayOutputStream outputStreamCaptor;
    private Environment environment;

    @BeforeEach
    public void setup() {
        environment = new Environment();
        outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void after() {
        System.setOut(stdin);
        try {
            outputStreamCaptor.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void placeInTheSamePlace() {
        Assertions.assertThrows(EnvironmentException.class, () -> {
            ComputerBank computerBank = new ComputerBank();
            ComputerBank computerBank2 = new ComputerBank();
            environment.place(0, 0, computerBank);
            environment.place(0, 0, computerBank2);
        });
    }

    @Test
    public void computerBankTest() {
        ComputerBank computerBank = new ComputerBank();
        environment.place(100, 100, computerBank);
        environment.play();
        Assertions.assertEquals(
                "[Комьютерный банк: работает]",
                outputStreamCaptor.toString().trim()
        );
    }

    @Test
    public void bombTest() {
        Bomb bomb = new Bomb(200);
        environment.place(100, 100, bomb);
        environment.play();
        Assertions.assertEquals(
                "[Бомба взорвана]",
                outputStreamCaptor.toString().trim()
        );
    }

    @Test
    public void computerBankExplosion() {
        environment.place(95, 105, new Bomb(200));
        environment.place(100, 100, new ComputerBank());
        environment.play();
        Assertions.assertEquals(
                "[Бомба взорвана]" + "\n" + "[Комьютерный банк: ...]",
                outputStreamCaptor.toString().trim()
        );
    }

    @Test
    public void bombsTooFarTest() {
        environment.place(100, 120, new Bomb(100));
        environment.place(120, 100, new Bomb(100));
        environment.place(100, 100, new ComputerBank());
        environment.play();
        Assertions.assertEquals(
                """
                        [Бомба взорвана]
                        [Бомба взорвана]
                        [Комьютерный банк: работает]""",
                outputStreamCaptor.toString().trim()
        );
    }

    @Test
    public void executionOrderTest() {
        environment.place(100, 100, new ComputerBank());
        environment.place(105, 100, new Bomb(100));
        environment.play();
        Assertions.assertEquals(
                """
                        [Комьютерный банк: работает]
                        [Бомба взорвана]
                        [Комьютерный банк: ...]""",
                outputStreamCaptor.toString().trim()
        );
    }

    @Test
    public void chainExplosion() {
        environment.place(100, 105, new Bomb(60));
        environment.place(100, 100, new ComputerBank());
        environment.place(105, 100, new Bomb(60));
        environment.play();
        Assertions.assertEquals(
                """
                        [Бомба взорвана]
                        [Бомба взорвана]
                        [Комьютерный банк: ...]""",
                outputStreamCaptor.toString().trim()
        );
    }
}
