import org.example.Utils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarketTest {
    private List<RemoteWebDriver> drivers;
    public final String CATEGORY = "Навигаторы";

    @BeforeEach
    public void setup() {
        drivers = Utils.loadDrivers();
    }

    @AfterEach
    public void quit() {
        drivers.forEach(RemoteWebDriver::quit);
    }

    @Test
    void findSomethingInMarketForEachDriver() {
        drivers.forEach(this::findSomethingInMarket);
    }

    void findSomethingInMarket(RemoteWebDriver driver) {
        driver.get(Utils.BASE_URL);
        driver.manage().window().setSize(new Dimension(1756, 956));
        driver.findElement(By.xpath("//a[contains(@href, '/experience/mercedes/')]")).click();
        Utils.waitLoading();
        driver.findElement(By.xpath("//div[@class='c-nav-list']/a[@href='/market']")).click();
        Utils.waitLoading();

        new Actions(driver).moveToElement(
                driver.findElement(By.xpath(
                        "//ul/li/span/a[contains(text(),'"+CATEGORY+"')]"))
        ).click().perform();
        Utils.waitLoading();
        var element = driver.findElement(By.xpath(
                "(//div[contains(@class, 'offer-card offer-card--wide')])[5]"));
        String title = element.findElement(
                By.xpath("(//div/div/span[contains(@class, 'c-link')])[5]")).getText();
        new Actions(driver).moveToElement(element).click().perform();
        Utils.waitLoading();
        assertEquals(title, driver.findElement(By.xpath("//span[@itemprop='name']")).getText());
    }

}
