import org.example.Utils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SeleniumTest {
    final List<RemoteWebDriver> drivers = Utils.loadDrivers();

    @Test
    void openForEachDriver() {
        drivers.forEach(this::openRemoteResource);
    }

    void openRemoteResource(RemoteWebDriver driver) {
        driver.get(Utils.BASE_URL);
        assertEquals(driver.getTitle(), "DRIVE2.RU");
        driver.quit();
    }

}
