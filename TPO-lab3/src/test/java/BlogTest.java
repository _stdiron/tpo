import org.example.Utils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BlogTest {
    private List<RemoteWebDriver> drivers;
    public final String VENDOR = "lada";
    public final String ENGINE = "1";
    public final String MODEL = "1813";

    @BeforeEach
    public void setup() {
        drivers = Utils.loadDrivers();
    }

    @AfterEach
    public void quit() {
        drivers.forEach(RemoteWebDriver::quit);
    }

    @Test
    void openPostOfFilteredCategoryForEachDriver() {
        drivers.forEach(this::openPostOfFilteredCategory);
    }

    void openPostOfFilteredCategory(RemoteWebDriver driver) {
        driver.get(Utils.BASE_URL);
//        driver.manage().window().setSize(new Dimension(1756, 956));
        driver.findElement(By.xpath("//a[contains(@href, '/experience/" + VENDOR + "/')]")).click();
        Utils.waitLoading();
        var select = new Select(driver.findElement(By.xpath("//select[@name='ModelId']")));
        select.selectByValue(MODEL);

        select = new Select(driver.findElement(By.xpath("//select[@name='Engine']")));
        select.selectByValue(ENGINE);

        driver.findElement(By.xpath("//form[@id='experience']/div[8]/button")).click();
        Utils.waitLoading();

        var result = driver.findElement(By.xpath("(//div[@class='c-post-preview__title']/a)[2]"));
        var href = result.getDomProperty("href");
        var oldTab = driver.getWindowHandle();
        new Actions(driver).moveToElement(result).click().perform();
        Utils.switchNewTab(driver, oldTab);
        Utils.waitLoading();
        assertEquals(href, driver.getCurrentUrl());
        new Actions(driver).moveToElement(
                driver.findElement(By.xpath("//div[@id='comments']"))
        ).perform();
        Utils.waitLoading();
    }

}
