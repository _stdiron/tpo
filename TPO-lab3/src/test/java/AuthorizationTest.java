import org.example.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AuthorizationTest {
    private List<RemoteWebDriver> drivers;

    @BeforeEach
    public void setup() {
        drivers = Utils.loadDrivers();
    }

    @Test
    void authorizeAttemptForEachDriver() {
        drivers.forEach(this::tryToAuthorizeWithWrongCredentials);
    }

    @Test
    void authorizationForEachDriver() {
        drivers.forEach(this::authorizeAndLogout);
    }

    void tryToAuthorizeWithWrongCredentials(RemoteWebDriver driver) {
        // open and configure
        driver.get(Utils.BASE_URL);
        driver.manage().window().setSize(new Dimension(1756, 956));
        // authorization attempt
        driver.findElement(By.xpath("//a[contains(@href, '/reception/')]")).click();
        driver.findElement(By.xpath("//input[@name='Login']")).click();
        driver.findElement(By.xpath("//input[@name='Login']")).sendKeys("GTX.1060@yandex.ru");
        driver.findElement(By.xpath("//input[@name='Password']")).click();
        driver.findElement(By.xpath("//input[@name='Password']")).sendKeys("WrongPass123");
        driver.findElement(By.xpath("//form[@id='loginForm']/div[3]/label")).click();
        driver.findElement(By.xpath("//form[@id='loginForm']/div[4]/div/div/button")).click();
        WebElement errorSpan = driver.findElement(By.xpath("//span[@class='field-validation-error']"));
        System.out.println("message: " + errorSpan.getText());
        assertEquals("Указан неверный логин или пароль", errorSpan.getText());
        driver.quit();
    }

    void authorizeAndLogout(RemoteWebDriver driver) {
        driver.get(Utils.BASE_URL);
        driver.manage().window().setSize(new Dimension(1756, 956));

        driver.findElement(By.xpath("//a[contains(@href, '/reception/')]")).click();
        driver.findElement(By.xpath("//input[@name='Login']")).click();
        driver.findElement(By.xpath("//input[@name='Login']")).sendKeys("GTX.1060@yandex.ru");
        driver.findElement(By.xpath("//input[@name='Password']")).click();
        driver.findElement(By.xpath("//input[@name='Password']")).sendKeys("TestPassword123");
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();

        // check if authorized successfully
        assertEquals("DRIVE2.RU", driver.getTitle());
        driver.findElement(By.xpath("//a/x-img/img")).click();
        assertTrue(driver.getTitle().contains("Vlad64112"));

        // logout
        driver.findElement(By.xpath("//button[2]/div")).click();
        driver.findElement(By.xpath("//form/button")).click();
        assertEquals("DRIVE2.RU", driver.getTitle());

        driver.quit();
    }

}
