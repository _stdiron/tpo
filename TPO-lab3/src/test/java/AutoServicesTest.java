import org.example.Utils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class AutoServicesTest {
    private List<RemoteWebDriver> drivers;
    public final int VENDOR_ID = 19;

    @BeforeEach
    public void setup() {
        drivers = Utils.loadDrivers();
    }

    @AfterEach
    public void quit() {
        drivers.forEach(RemoteWebDriver::quit);
    }

    @Test
    void openServiceForSpecialVendorForEachDriver() {
        drivers.forEach(this::openServiceForSpecialVendor);
    }

    void openServiceForSpecialVendor(RemoteWebDriver driver) {
        driver.get(Utils.BASE_URL);
        driver.manage().window().setSize(new Dimension(1756, 956));
        driver.findElement(By.xpath("//a[contains(@href, '/experience/mercedes/')]")).click();
        Utils.waitLoading();
        driver.findElement(By.xpath("//div[@class='c-nav-list']/a[@href='/companies']")).click();
        Utils.waitLoading();

        var select = new Select(driver.findElement(By.xpath("//select[@name='brand']")));
        select.selectByIndex(VENDOR_ID);

        driver.findElement(By.xpath("//form[@action='/companies']/button[@type='submit']")).click();
        Utils.waitLoading();

        var oldTab = driver.getWindowHandle();
        driver.findElement(By.xpath("(//header/h3/a[@class='u-card-link'])[1]")).click();
        Utils.switchNewTab(driver, oldTab);
        Utils.waitLoading();

        new Actions(driver).scrollToElement(
            driver.findElement(By.xpath("//h3[contains(text(),'Блог')]"))
        ).scrollToElement(
            driver.findElement(By.xpath("//h3[contains(text(),'Отзывы')]"))
        ).scrollToElement(
            driver.findElement(By.xpath("//h3[contains(text(),'Контакты')]"))
        ).perform();
        Utils.waitLoading();
    }

}
