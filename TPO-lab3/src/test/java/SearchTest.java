import org.example.Utils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class SearchTest {

    private List<RemoteWebDriver> drivers;

    @BeforeEach
    public void setup() {
        drivers = Utils.loadDrivers();
    }

    @AfterEach
    public void quit() {
        drivers.forEach(RemoteWebDriver::quit);
    }

    @Test
    void noResultsSearchForEachDriver() {
        drivers.forEach(this::noResultsSearch);
    }

    @Test
    void successfulSearchForEachDriver() {
        drivers.forEach(this::successfulSearch);
    }

    void noResultsSearch(RemoteWebDriver driver) {
        driver.get(Utils.BASE_URL);
//        driver.manage().window().setSize(new Dimension(1756, 956));
        driver.findElement(By.xpath("//input[@name='text' and @type='search']")).click();
        driver.findElement(By.xpath("//input[@name='text' and @type='search']")).sendKeys(
                "asdasadassfadfsadavfsdfvffvfrvrvercecfwedwedwed"
        );
        driver.findElement(By.xpath("//input[@name='text' and @type='search']")).sendKeys(Keys.ENTER);
        Utils.waitLoading();
        var result = driver.findElement(By.xpath("//div[@class='c-empty-content__header']"));
        assertTrue(result.getText().contains("К сожалению, по вашему запросу ничего не нашлось"));
    }

    void successfulSearch(RemoteWebDriver driver) {
        driver.get(Utils.BASE_URL);
//        driver.manage().window().setSize(new Dimension(1756, 956));
        driver.findElement(By.xpath("//input[@name='text' and @type='search']")).click();
        driver.findElement(By.xpath("//input[@name='text' and @type='search']")).sendKeys(
                "бмв м3 компетишн 2023"
        );
        driver.findElement(By.xpath("//input[@name='text' and @type='search']")).sendKeys(Keys.ENTER);
        Utils.waitLoading();

        var result = driver.findElement(By.xpath("(//header/a[@class='u-card-link'])[1]"));
        var href = result.getDomProperty("href");
        var oldTab = driver.getWindowHandle();
        result.click();
        Utils.switchNewTab(driver, oldTab);
        Utils.waitLoading();
        assertEquals(href, driver.getCurrentUrl());
    }
}
