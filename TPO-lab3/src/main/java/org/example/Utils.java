package org.example;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static final String BASE_URL = "https://www.drive2.ru/";
    public static final int TIMEOUT = 2000;
    public static List<RemoteWebDriver> loadDrivers() {
        return List.of(new ChromeDriver());
    }

    public static void waitLoading() {
        try {
            Thread.sleep(TIMEOUT);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void switchNewTab(RemoteWebDriver driver, String oldHandle) {
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        newTab.remove(oldHandle);
        driver.switchTo().window(newTab.get(0));
    }
}
